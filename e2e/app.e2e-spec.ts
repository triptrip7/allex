import { AllExPage } from './app.po';

describe('all-ex App', function() {
  let page: AllExPage;

  beforeEach(() => {
    page = new AllExPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
