import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { UserComponent } from './users/user/user.component';
import {AngularFireModule} from 'angularfire2'; 

import {UsersService} from './users/users.service';
import {PostsService} from './posts/posts.service';
import { PostComponent } from './posts/post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { PostFormComponent } from './posts/post-form/post-form.component';

const appRoutes:Routes = [
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path: '', component:UsersComponent},
  {path: '**', component:PageNotFoundComponent}
]

export const firebaseConfig = {
    apiKey: "AIzaSyBpImyFVESixMzFeKaASNVxe8cIjSIdBRM",
    authDomain: "allex-6900b.firebaseapp.com",
    databaseURL: "https://allex-6900b.firebaseio.com",
    storageBucket: "allex-6900b.appspot.com",
    messagingSenderId: "1050989055208"
}

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService , PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
