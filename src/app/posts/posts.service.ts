import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {AngularFire} from 'angularfire2';


@Injectable()
export class PostsService {

 postsObservable;

  getPosts(){
   this.postsObservable = this.af.database.list('/posts').map(
     posts =>{
       posts.map(
         post => {
           post.authorNames = [];
           for (var userKey in post.authors){
             post.authorNames.push(
               this.af.database.object('/users/'+userKey)
             )
           }
         }
       );
       return posts;
     }
   );
   return this.postsObservable;
  }
  

  addPost(post){
    this.postsObservable.push(post);
  }

  deletePost(post){
    this.af.database.object('/posts/'+post.$key).remove(); 
  }

  updatePost(post){
    let postData = {title:post.title , author:post.author , body:post.body};
    this.af.database.object('/posts/'+post.$key).update(postData);
  }

  constructor(private af:AngularFire) { }

}
