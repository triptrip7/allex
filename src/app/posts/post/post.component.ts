import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs: ['post']
})
export class PostComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() updateEvent = new EventEmitter<Post>();
  post:Post;
  editButtonText:String="Edit";
  isEdit:Boolean=false;
  oldPost:Post;

  toggleEdit(){
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
    if(this.isEdit){
      this.oldPost=Object.assign({},this.post);
    }
    if(!this.isEdit){
      this.updateEvent.emit(this.post);
    }
  }

  cancelEdit(){
    this.post=Object.assign({},this.oldPost);
    this.toggleEdit();  
  }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  constructor() { }

  ngOnInit() {
  }

}
