import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;

  currentPost;

  isLoading:Boolean=true;

  changeCurrentPost(post){
    this.currentPost = post;
  }

  addPost(post){
    this._postsService.addPost(post);
  }

  deletePost(post){
    this._postsService.deletePost(post);
  }

  updatePost(post){
    this._postsService.updatePost(post);
  }

  constructor(private _postsService:PostsService) { }

  ngOnInit() {
     //this.posts = this._postsService.getPosts();
     this._postsService.getPosts().subscribe(postData => {
       this.posts = postData;
       this.isLoading = false;
    });

  }

}
