import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from '../post/post';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Output() postAddEvent = new EventEmitter<Post>();
  post:Post={title:'', author:'', body:''};
  
  onSubmit(form:NgForm){
    this.postAddEvent.emit(this.post);
    this.post={
      title:'',
      author:'',
      body:''
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
