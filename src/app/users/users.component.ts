import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;

  currentUser;

  isLoading:Boolean=true;

  changeCurrentUser(user){
    this.currentUser = user;
  }

  addUser(user){
    this._usersService.addUser(user);
  }

  deleteUser(user){
    this._usersService.deleteUser(user);
  }

  updateUser(user){
    this._usersService.updateUser(user);
  }

  constructor(private _usersService:UsersService) { }

  ngOnInit() {
    //this.users = this._usersService.getUsers();
    this._usersService.getUsers().subscribe(userData => {
      this.users = userData;
      this.isLoading = false;
  });
  }

}
