import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs: ['user']
})
export class UserComponent implements OnInit {

   @Output() deleteEvent = new EventEmitter<User>();
   @Output() updateEvent = new EventEmitter<User>();
  user:User;
  editButtonText:String="Edit";
  isEdit:Boolean=false;
  oldUser:User;

  toggleEdit(){
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
    if(this.isEdit){
      this.oldUser=Object.assign({},this.user);
    }
    if(!this.isEdit){
      this.updateEvent.emit(this.user);
    }
  }

  cancelEdit(){
    this.user=Object.assign({},this.oldUser);
    this.toggleEdit();  
  }

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  constructor() { }

  ngOnInit() {
  }

}
