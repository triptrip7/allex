import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {

  usersObservable;

  getUsers(){
   this.usersObservable = this.af.database.list('/users').map(
     users =>{
       users.map(
         user => {
           user.postTitles = [];
           for (var postKey in user.titles){
             user.postTitles.push(
               this.af.database.object('/posts/'+postKey)
             )
           }
         }
       );
       return users;
     }
   );
   return this.usersObservable;
  }

  addUser(user){
    this.usersObservable.push(user);
  }

  deleteUser(user){
    this.af.database.object('/users/'+user.$key).remove(); 
  }

  updateUser(user){
    let userData = {name:user.name , email:user.email};
    this.af.database.object('/users/'+user.$key).update(userData);
  }

  constructor(private af:AngularFire) { }

}
